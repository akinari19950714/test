document.getElementById("hello_text").textContent = "はじめてのJavaScript";

const R="20";//行数
const C="10";//列数

var count=0;
//var cells;//外部で宣言して関数内で配列にしても他の関数で使えない？
var cells=new Array(R);//1次元配列生成
for(var i=0;i<R;i++) cells[i]=new Array(C);//二次元に拡張->シンプルにしたい
//ブロック生成（構造体ぽい？）
var blocks=
{
	i:{class:"i",pattern:[[1,1,1,1]]},
	o:{class:"o",pattern:[[1,1],[1,1]]},
	t:{class:"t",pattern:[[0,1,0],[1,1,1]]},
	s:{class:"s",pattern:[[0,1,1],[1,1,0]]},
	z:{class:"z",pattern:[[1,1,0],[0,1,1]]},
	j:{class:"j",pattern:[[1,0,0],[1,1,1]]},
	l:{class:"l",pattern:[[0,0,1],[1,1,1]]}
};
var isFalling=false;//ブロックが落下中かどうかの変数
var fallingBlockNum=0;//

main();

function main()
{	
	loadTable();
	setInterval(func,500);
}


function loadTable()
{
	var td_array=document.getElementsByTagName("td");
	//var cells=[R][C];//二次元配列の初期宣言はできない？
	//var cells=new Array(R);//1次元配列生成
	var index=0;	
	var row, col;
	
	for(row=0;row<R;row++)
	{
		//cells[row]=new Array(C);//二次元に拡張->シンプルにしたい
		for(col=0;col<C;col++)
	 {
	 	cells[row][col]=td_array[index];//1次元データを二次元にする
	 	index++;
	 }
	}
}


function func()
{
	count++;
	document.getElementById("hello_text").textContent = "はじめてのJavaScript(" + count + ")";
	//GameOverCheck();
	document.addEventListener("keydown",onKeyDown);
	if(isFalling) fallBlocks();//落下中のブロックがあれば落とす
	else
	{
	deleteRow();//揃っている行を消す
	generateBlock();//ブロック生成
	}	
}


function fallBlocks()
{
	var row, col;
	/*
	// 一番下の行のクラスを空にする
	for(col=0;col<C;col++)
	cells[R-1][col].className="";
	*/
	// 1. 底についていないか？
	for(col=0;col<C;col++)
	if(cells[R-1][col].blockNum===fallingBlockNum)
	{
	isFalling=false;
	return;//一番下の行にブロックがいるので落とさない
	}
	
	// 2. 1マス下に別のブロックがないか？
	for(row=R-2;row>=0;row--)
	for(col=0;col<C;col++)
	if(cells[row][col].blockNum===fallingBlockNum)
	if(cells[row+1][col].className!==""&&cells[row+1][col].blockNum!==fallingBlockNum)
	{
	isFalling=false;	
	return;//１つ下のマスにブロックがいるので落とさない
	}
	
	// 下から二番目の行から繰り返しクラスを下げていく
	for(row=R-2;row>=0;row--)
	for(col=0;col<C;col++)
	if(cells[row][col].blockNum===fallingBlockNum)
	{
		cells[row + 1][col].className = cells[row][col].className;
        cells[row + 1][col].blockNum = cells[row][col].blockNum;
        cells[row][col].className = "";
        cells[row][col].blockNum = null;	
	}
	
	/*
	if(cells[row][col].className !== "")
	{
	cells[row+1][col].className=cells[row][col].className;
	cells[row][col].className="";
	}
	*/
}


function generateBlock()
{
	GameOverCheck();
	var row, col;
	// ランダムにブロックを生成する
	// 1. ブロックパターンからランダムに一つパターンを選ぶ
	var key=Object.keys(blocks);
	var nextBlockKey=key[Math.floor(Math.random()*key.length)];
	var nextBlock=blocks[nextBlockKey];
	var nextFallingBlockNum=fallingBlockNum+1;
	
	// 2. 選んだパターンをもとにブロックを配置する
	var pattern = nextBlock.pattern;
	for (var row = 0; row<pattern.length;row++) 
	for (var col = 0; col<pattern[row].length; col++)
      if (pattern[row][col]) 
      {
      	cells[row][col + 3].className = nextBlock.class;
      	cells[row][col+3].blockNum=nextFallingBlockNum;
      }
      
      // 3. 落下中のブロックがあるとする
      isFalling=true;
      fallingBlockNum=nextFallingBlockNum;
      
}


function onKeyDown(event)
{
	if(event.keyCode===37) moveLeft();//左移動
	else if(event.keyCode===39) moveRight();//右移動
	else if(event.keyCode===40) fallBlocks();//下移動
	//else if(event.keyCode===38) rot();//回転
}


function moveRight()
{
	var row,col;
	
	//端にいってないか	
	for(row=0;row<R;row++)
	if(cells[row][9].blockNum===fallingBlockNum) return;
	
	//隣にブロックはないか
	for(row=0;row<20;row++)
	for(col=C-1;col>=0;col--)
	if(cells[row][col].blockNum===fallingBlockNum&&cells[row][col+1].className!==""&&cells[row][col+1].blockNum!==fallingBlockNum) return;
	
	//ずらす
	for(row=0;row<20;row++)
	for(col=C-1;col>=0;col--)
	if(cells[row][col].blockNum===fallingBlockNum)
	{
		cells[row][col + 1].className = cells[row][col].className;
        cells[row][col + 1].blockNum = cells[row][col].blockNum;
        cells[row][col].className = "";
        cells[row][col].blockNum = null;	
	}
}


function moveLeft()
{
	var row,col;
	
	//端にいってないか
	for(row=0;row<R;row++)
	if(cells[row][0].blockNum===fallingBlockNum) return;
	
	//隣にブロックはないか
	for(row=0;row<20;row++)
	for(col=C-1;col>=0;col--)
	if(cells[row][col].blockNum===fallingBlockNum&&cells[row][col-1].className!==""&&cells[row][col-1].blockNum!==fallingBlockNum) return;
	
	//ずらす
	for(row=0;row<R;row++)
	for(col=0;col<C;col++)
	if(cells[row][col].blockNum===fallingBlockNum)
	{
		cells[row][col-1].className = cells[row][col].className;
        cells[row][col-1].blockNum = cells[row][col].blockNum;
        cells[row][col].className = "";
        cells[row][col].blockNum = null;
     }	
}


function deleteRow()
{
	var row,col;
	for(row=0;row<20;row++)
	{
		var canDelete=true;
		for(col=C-1;col>=0;col--)
		if(cells[row][col].className==="") canDelete=false;
		
		//1行消す
		if(canDelete)
		{
			for(col=0;col<C;col++) cells[row][col].className="";	
		
		//上の行を落とす
		for(var downRow=row-1;downRow>=0;downRow--)
		for(col=0;col<C;col++)
		 {
			cells[downRow + 1][col].className = cells[downRow][col].className;
			cells[downRow + 1][col].blockNum = cells[downRow][col].blockNum
			cells[downRow][col].className = "";
			cells[downRow][col].blockNum = null;
		 }
		}
	}
}


function GameOverCheck()
{
	var row,col;
	var f=0;
	for(row=0;row<1;row++)
	for(col=0;col<C;col++)
	if(cells[row][col].className!=="") f=1;
	
	if(f===1)
	{
	alert("Game Over");	
	//何かしらのアクション，例えばゲーム版を初期化．
	}
}


function rot()
{
	var row,col;
	var x,y=10;
	var i,j;
	var f=0;
	//var temp=new Array(4);
	//for(var i=0;i<4;i++) temp[i]=new Array(4);
	var temp=cells;
	
	//ブロックが含まれる4*4のうち左上
	for(row=0;row<R;row++)
	for(col=0;col<C;col++)
	if(cells[row][col].blockNum===fallingBlockNum)
	{
		if(f===0) {x=row; f=1;}
		if(y>col) y=col;
	} 
	
	//4*4内に他のブロックは入っていないか？
	
	
	//別の配列に回転させたのを一時保存
	//if()
	for(i=0;i<4;i++)
	for(j=0;j<4;j++)
	{
		temp[x+j][C-(y+i)].className=cells[x+i][y+j].className;
		temp[x+j][C-(y+i)].blockNum=cells[x+i][y+j].blockNum;
		cells[x+i][y+j].className="";
		cells[x+i][y+j].blockNum=null;
	}//うまくいかない
	
	//回転させたのを入れる
	//cells=temp;
	for(i=0;i<4;i++)
	for(j=0;j<4;j++)
	{
		cells[x+i][y+j].blockNum=temp[x+i][y+j].blockNum;
		cells[x+i][y+j].className=temp[x+i][y+j].className;	
	}
	
	/*
	for(i=0;i<R;i++)
	for(j=0;j<C;j++)
	{
		temp[i][j].className="";
		temp[i][j].blockNum=null;
	}
	*/
	//alert(x);
	//alert(y);
}